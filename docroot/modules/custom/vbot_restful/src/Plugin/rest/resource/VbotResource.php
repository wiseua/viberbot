<?php

namespace Drupal\vbot_restful\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Viber\Bot;
use Viber\Api\Sender;
use Viber\Api\Response;
/**
 * Provides a Vbot Resource
 *
 * @RestResource(
 *   id = "vbot_resource",
 *   label = @Translation("Vbot Resource"),
 *   uri_paths = {
 *     "canonical" = "/restfulsend",
 *     "https://www.drupal.org/link-relations/create" = "/restfulsend"
 *   }
 * )
 */
class VbotResource extends ResourceBase {
  /**
   * Responds to entity GET requests.
   * @return \Drupal\rest\ResourceResponse
   */
   
   public static function sender($id, $message) {
    $apiKey = \Drupal::state()->get('apiKey');
    $botSender = new Sender([
      'name' => 'Wise bot',
      'avatar' => 'https://www.shareicon.net/data/128x128/2016/11/14/852211_drupal_512x512.png',
    ]);
    $bot = new Bot(['token' => $apiKey]);
    $log = \Drupal::logger('VbotRest');
    
        $bot->getClient()->sendMessage(
          (new \Viber\Api\Message\Text())
            ->setSender($botSender)
            ->setReceiver($id)
            ->setText($message)
        );
  }
   
  public function post($data) {
    $log = \Drupal::logger('VbotRest');
    if (isset($data['message']['id'])) {
      $this->sender($data['message']['id'], $data['message']['text']);
    }
    else {
      foreach ($data['message'] as $value) {
        $this->sender($value['id'], $value['text']);
      }
    }
    //   $client = new \Viber\Client(['token' => '4631b21a6075c4f7-05bb7d90c4be55f6-c0b8f3c5574ac021']);
      
    // $log->notice('<pre>' . print_r($client->getAccountInfo(), true) . '</pre>');
    $response = ['message' => 'Success!'];
    return new ResourceResponse($response);
    
  }
}