<?php
/**
 * Created by PhpStorm.
 * User: misha
 * Date: 09.09.17
 * Time: 14:03
 */
namespace Drupal\vbot;
use Drupal\node\Entity\Node;

class importClients extends sendMessageBatch{

  public function processItem($phone, $inviteCode, &$context) {
    $log = \Drupal::logger('ViberBot');
    if(!empty($phone) && !empty($inviteCode)) {
      $db = \Drupal::database();
      if (preg_match('/0[0-9]{9}/', $phone, $matches)) {
        $ph = $db->select('node__field_phone_number', 'ph')
          ->fields('ph', ['field_phone_number_value', 'entity_id'])
          ->condition('field_phone_number_value', $matches[0])
          ->execute()
          ->fetchAllKeyed(1, 0);
        if (empty($ph)) {
          $node = Node::create([
            'type' => 'viber_user',
            'field_user_id' => 'none',
            'field_user_name' => 'none',
            'title' => 'none',
            'field_phone_number' => $matches[0],
            'field_invite_code' => $inviteCode,
            'created' => time(),
          ]);
          $node->save();
          $context['results'][] = $matches[0];
          $context['message'] = 'Sending message...';
        }
      }
    }
  }
}