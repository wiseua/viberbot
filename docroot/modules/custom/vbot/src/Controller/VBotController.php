<?php

namespace Drupal\vbot\Controller;

use Drupal\Core\Executable\ExecutableInterface;
use Drupal\node\Entity\Node;
use Viber\Bot;
use Viber\Api\Sender;


class VBotController implements ExecutableInterface {

  private function registration($event){
    if (preg_match('/0[0-9]{9}/', $event->getMessage()->getText(), $matches)) {
      $db = \Drupal::database();
      $nodes = $db->select('node__field_phone_number', 'ph')
        ->fields('ph', ['field_phone_number_value', 'entity_id'])
        ->condition('field_phone_number_value', $matches[0])
        ->execute()
        ->fetchAllKeyed(1, 0);
      if (!empty($nodes)) {
        $node = Node::load(key($nodes));
        $node->set("field_user_id", $event->getSender()->getId());
        $node->set("field_user_name", $event->getSender()->getName());
        $node->set("title", $event->getSender()->getName());
        $node->save();
      }
      else{
        $node = Node::create([
          'type' => 'viber_user',
          'field_user_id' => $event->getSender()->getId(),
          'field_user_name' => $event->getSender()->getName(),
          'title' => $event->getSender()->getName(),
          'field_phone_number' => $matches[0],
          'created' => time(),
        ]);
        $node->save();
      }
    }
  }

  public function execute() {

    $apiKey = \Drupal::state()->get('apiKey');

    $botSender = new Sender([
      'name' => 'Wise bot',
      'avatar' => 'https://www.shareicon.net/data/128x128/2016/11/14/852211_drupal_512x512.png',
    ]);

// log bot interaction
    $log = \Drupal::logger('ViberBot');

    $bot = NULL;

    try {
      // create bot instance
      $bot = new Bot(['token' => $apiKey]);
      $bot
        ->onConversation(function ($event) use ($bot, $botSender, $log) {
          $log->notice('onConversation handler');

//          $db = \Drupal::database();
//          $nodes = $db->select('node__field_user_id','id')
//            ->fields('id', ['field_user_id_value', 'entity_id'])
//            ->condition('field_user_id_value', $event->getUser()->getId())
//            ->execute()
//            ->fetchAllKeyed(1,0);
//          if (!empty($nodes)) {
//            $bot->getClient()->sendMessage(
//              (new \Viber\Api\Message\Text())
//                ->setSender($botSender)
//                ->setReceiver($event->getUser()->getId())
//                ->setText(t('Your subscribed!'))
//            );
//          }
//          else {
            $bot->getClient()->sendMessage(
              (new \Viber\Api\Message\Text())
                ->setSender($botSender)
                ->setReceiver($event->getUser()->getId())
                ->setText(t('Enter an invitation code or phone number to subscribe!'))
            );
//          }
        })
        ->onText('/phone.*/', function ($event) use ($bot, $botSender, $log) {
          $log->notice(print_r($event->getMessage()->getText(), TRUE));
          $bot->getClient()->sendMessage(
            (new \Viber\Api\Message\Text())
            ->setSender($botSender)
            ->setReceiver($event->getSender()->getId())
            ->setMinApiVersion(3)
            ->setText("We need your phone number")
            ->setKeyboard(
            (new \Viber\Api\Keyboard())
                ->setButtons([
                    (new \Viber\Api\Keyboard\Button())
                            ->setActionType('share-phone')
                            ->setActionBody('reply')
                            ->setText('Send phone number')
                        ])
            )
          
          );
          $log->notice(print_r($event->getMessage()->getPhoneNumber(), TRUE));
        })
        ->onText('/Send phone number.*/', function ($event) use ($bot, $botSender, $log) {
          $log->notice(print_r($event->getMessage()->getText(), TRUE));
        })
        ->onText('/^[0-9]{5}$/', function ($event) use ($bot, $botSender, $log) {
          // это событие будет вызвано если пользователь пошлет сообщение
          // которое совпадет с регулярным выражением
          $db = \Drupal::database();
          $nodes = $db->select('node__field_invite_code','inv')
            ->fields('inv', ['field_invite_code_value', 'entity_id'])
            ->condition('field_invite_code_value', $event->getMessage()->getText())
            ->execute()
            ->fetchAllKeyed(1,0);
          if (!empty($nodes)) {
            $node = Node::load(key($nodes));
            $node->set("field_user_id", $event->getSender()->getId());
            $node->set("field_user_name", $event->getSender()->getName());
            $node->set("title", $event->getSender()->getName());
            $node->save();
            $bot->getClient()->sendMessage(
              (new \Viber\Api\Message\Text())
                ->setSender($botSender)
                ->setReceiver($event->getSender()->getId())
                ->setText("Thanks for subscribing to our public!")
            );
          }
          else {
            $log->notice('<pre>' . print_r('Not found', TRUE) . '</pre>');
            $bot->getClient()->sendMessage(
              (new \Viber\Api\Message\Text())
                ->setSender($botSender)
                ->setReceiver($event->getSender()->getId())
                ->setText("For the subscription enter the correct invitation code!")
            );
          }
        })
        ->onText('/38[0-9]{10}$/', function ($event) use ($bot, $botSender, $log) {
          $this->registration($event);
          $bot->getClient()->sendMessage(
            (new \Viber\Api\Message\Text())
              ->setSender($botSender)
              ->setReceiver($event->getSender()->getId())
              ->setText("Thanks for subscribing to our public!")
          );
        })
        ->onText('/0[0-9]{9}$/', function ($event) use ($bot, $botSender, $log) {
          $this->registration($event);
          $bot->getClient()->sendMessage(
            (new \Viber\Api\Message\Text())
              ->setSender($botSender)
              ->setReceiver($event->getSender()->getId())
              ->setText("Thanks for subscribing to our public!")
          );
        })
        ->run();


    } catch (\Exception $e) {
      $log->error('Exception: ' . $e->getMessage());
      if ($bot) {
        $log->error('Actual sign: ' . $bot->getSignHeaderValue());
        $log->error('Actual body: ' . $bot->getInputBody());
      }
    }


    $output = [];

    $output['#title'] = 'HelloWorld page title';

    $output['#markup'] = 'Hello World!';

    return $output;
  }

}
