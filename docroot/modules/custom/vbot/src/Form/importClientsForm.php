<?php
/**
 * Created by PhpStorm.
 * User: misha
 * Date: 09.09.17
 * Time: 14:06
 */

namespace Drupal\vbot\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use Drupal\vbot\importClients;

class importClientsForm extends FormBase{
  public function getFormId() {
    return 'importClientsForm';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['file'] = [
      '#type' => 'managed_file',
      '#upload_location' => 'public://vbot_csv/',
      '#multiple' => FALSE,
      '#upload_validators' => [
        'file_validate_extensions' => ['csv'],
        'file_validate_size' => [25600000],
      ],
    ];
    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Import'),
    ];
    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $log = \Drupal::logger('ViberBot');
//    $oldCSVImportId = \Drupal::state()->get('oldCSVImportId');
//    if (!empty($oldCSVImportId)){
//      $oldCSVImportFile = File::load($oldCSVImportId);
//      if (file_exists($oldCSVImportFile->getFileUri())) {
//        $oldCSVImportFile->delete();
//      }
//      $log->notice('oldCSVImportId: ' . print_r($oldCSVImportId, true));
//    }
    $fid = $form_state->getValue('file')[0];
//    \Drupal::state()->set('oldCSVImportId', $fid);
    $import = new importClients($fid);
    $import->setBatch();
  }
}