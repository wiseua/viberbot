<?php

namespace Drupal\vbot\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Exception;
use Viber\Client;
use Viber\Api\Response;


class VBotSettings extends FormBase {


    public function getFormId() {
        return 'VBotSettings';
    }

    public function buildForm(array $form, FormStateInterface $form_state) {
        $form['api_key'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('API Key'),
            '#default_value' => \Drupal::state()->get('apiKey'));
  
        $form['actions']['#type'] = 'actions';

        $form['actions']['submit'] = array(
            '#type' => 'submit',
            '#value' => $this->t('Activate bot'),
            '#button_type' => 'primary',
        );
        return $form;
    }

    public function submitForm(array &$form, FormStateInterface $form_state) {
        \Drupal::state()->set('apiKey', $form_state->getValue('api_key'));
        $log = \Drupal::logger('VbotRest');
        $webhookUrl = 'https://v-bot-ace1995.c9users.io/vbot';

        try {
            $client = new Client([ 'token' => $form_state->getValue('api_key') ]);
            $client->setWebhook($webhookUrl);
            // $log->notice(print_r($client,true));
            drupal_set_message('Success', 'status');
        } catch (Exception $e) {
            drupal_set_message('Error ' . $e->getMessage() . '\n', 'error');
        }


    }

}