<?php


namespace Drupal\vbot\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use Drupal\vbot\sendMessageBatch;
use Viber\Bot;
use Viber\Api\Sender;

class VBotSendMessage extends FormBase {

  public function getFormId() {
    return 'VBotSendMessage';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['active'] = [
      '#type' => 'radios',
      '#title' => $this->t('Type'),
      '#options' => [
        0 => $this->t('<b>Individual mailing</b>'),
        1 => $this->t('<b>Mass mailing</b>'),
      ],
    ];

    $form['individual_mailing'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Individual mailing'),
      '#states' => [
        'visible' => [
          ':input[name="active"]' => ['value' => '0'],
        ],
      ],
    ];
    $form['individual_mailing']['type'] = [
      '#type' => 'radios',
      '#default_value' => 0,
      '#options' => [
        0 => $this->t('By user id'),
        1 => $this->t('By number phone'),
      ],
    ];
    $form['individual_mailing']['id_receiver'] = [
      '#type' => 'textfield',
      '#title' => $this->t('ID<font color="red"> *</font>'),
      '#states' => [
        'visible' => [
          ':input[name="type"]' => ['value' => '0'],
        ],
      ],
    ];
    $form['individual_mailing']['phone_receiver'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Phone number<font color="red"> *</font>'),
      '#states' => [
        'visible' => [
          ':input[name="type"]' => ['value' => '1'],
        ],
      ],
    ];
    $form['individual_mailing']['text_message'] = [
      '#type' => 'textarea',
      '#rows' => 8,
      '#title' => $this->t('Text message<font color="red"> *</font>'),
    ];
    $form['individual_mailing']['media'] = [
      '#title' => t('Upload image or video'),
      '#type' => 'managed_file',
      '#upload_location' => 'public://vbot_media/',
      '#multiple' => FALSE,
      '#description' => t('Allowed extensions: jpeg jpg mp4'),
      '#upload_validators' => [
        'file_validate_extensions' => ['jpeg jpg mp4'],
        'file_validate_size' => [25600000],
      ],
    ];
    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['individual_mailing']['actions']['submit_mass'] = [
      '#type' => 'submit',
      '#value' => t('Send'),
    ];
    $form['mass_mailing'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Mass mailing'),
      '#states' => [
        'visible' => [
          ':input[name="active"]' => ['value' => '1'],
        ],
      ],
    ];

    $form['mass_mailing']['file'] = [
      '#title' => t('Upload image or video'),
      '#type' => 'managed_file',
      '#upload_location' => 'public://vbot_csv/',
      '#description' => t('Allowed extensions: CSV'),
      '#multiple' => FALSE,
      '#upload_validators' => [
        'file_validate_extensions' => ['csv'],
        'file_validate_size' => [25600000],
      ],
    ];
    $form['mass_mailing']['actions']['submit_mass'] = [
      '#type' => 'submit',
      '#value' => t('Send'),
    ];
    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {

    if ($form_state->getValue('active') == 0) {
      if ($form_state->getValue('type') == 0) {
        if (empty($form_state->getValue('id_receiver'))) {
          $form_state->setErrorByName('id_receiver',
            $this->t('Fill in the required fields!'));
        }
      }
      else {
        if (empty($form_state->getValue('phone_receiver'))) {
          $form_state->setErrorByName('phone_receiver',
            $this->t('Fill in the required fields!'));
        }
      }
      if (empty($form_state->getValue('text_message'))) {
        $form_state->setErrorByName('text_message',
          $this->t('Fill in the required fields!'));
      }
    }
  }

  public static function sender($id, $message, $media = NULL, $type = 'text') {
    $apiKey = \Drupal::state()->get('apiKey');
    $botSender = new Sender([
      'name' => 'Wise bot',
      'avatar' => 'https://www.shareicon.net/data/128x128/2016/11/14/852211_drupal_512x512.png',
    ]);
    $bot = new Bot(['token' => $apiKey]);
    $log = \Drupal::logger('ViberBot');
    switch ($type){
      case 'text':
        $bot->getClient()->sendMessage(
          (new \Viber\Api\Message\Text())
            ->setSender($botSender)
            ->setReceiver($id)
            ->setText($message)
        );
      break;
      case 'jpeg':
        $bot->getClient()->sendMessage(
          (new \Viber\Api\Message\Picture())
            ->setSender($botSender)
            ->setReceiver($id)
            ->setText($message)
            ->setMedia($media)
        );
      break;
      case 'mp4':
        $bot->getClient()->sendMessage(
          (new \Viber\Api\Message\Video())
            ->setSender($botSender)
            ->setReceiver($id)
            ->setSize(25 * 1024 * 1024)
            ->setMedia($media)
        );
      break;
    }
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $log = \Drupal::logger('ViberBot');
    $url = NULL;
    $type = 'text';
    $id = NULL;
    $fid = NULL;
    $url = NULL;
//    $oldMediaID = \Drupal::state()->get('oldMediaId');
//    $oldCSVID = \Drupal::state()->get('oldCSVId');
    if ($form_state->getValue('active') == 0) {
//      if (!empty($oldMediaID)){
//        $oldMediaFile = File::load($oldMediaID);
//        if (file_exists($oldMediaFile->getFileUri())) {
//          $oldMediaFile->delete();
//        }
//        $log->notice('OldMedia: ' . print_r($oldMediaID, true));
//      }
      if ($form_state->getValue('type') == 0) {
        $id = $form_state->getValue('id_receiver');
        drupal_set_message('Success!');
      }
      else {
        $db = \Drupal::database();
        $nodes = $db->select('node__field_phone_number', 'ph')
          ->fields('ph', ['field_phone_number_value', 'entity_id'])
          ->condition('field_phone_number_value',
            $form_state->getValue('phone_receiver'))
          ->execute()
          ->fetchAllKeyed(1, 0);
        $viber_id = $db->select('node__field_user_id', 'id')
          ->fields('id', ['field_user_id_value', 'entity_id'])
          ->condition('entity_id', key($nodes))
          ->execute()
          ->fetchAllKeyed(0, 1);
        $id = key($viber_id);
        drupal_set_message('Success!');
      }
      if (!empty($form_state->getValue('media')[0])){
        $fid = $form_state->getValue('media')[0];
        $file = File::load($fid);
        $url = file_create_url($file->getFileUri());
        $str = explode('/', $file->getMimeType());
        $type = $str[1];
//        \Drupal::state()->set('oldMediaId', $fid);
      }
      $this->sender($id, $form_state->getValue('text_message'), $url, $type);

    }
    else {
//      if (!empty($oldCSVID)){
//        $oldCSVFile = File::load($oldCSVID);
//        if (file_exists($oldCSVFile->getFileUri())) {
//          $oldCSVFile->delete();
//        }
//        $log->notice('OldCSV: ' . print_r($oldCSVID, true));
//      }
      $fid = $form_state->getValue('file')[0];
//      \Drupal::state()->set('oldCSVId', $fid);
      $send = new sendMessageBatch($fid);
      $send->setBatch();
    }

  }
}